function limitFunctionCallCount(cb, n){
let get=1
function drop() {
    if(get<=n){
        console.log(` ${get}`)
        let p = cb()
        get++
        return p
    }
    return null
  }
  return drop
}
module.exports = limitFunctionCallCount