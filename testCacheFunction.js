const cacheFunction = require("./cacheFunction.js");
function cb(ted) {
    return ted * 2;
}
let ted = cacheFunction(cb);
console.log(ted(2, 4));
console.log(ted(2));