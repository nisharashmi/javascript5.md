function cacheFunction(cb) {
    let cache = {};
    const inner= set=> {
        if(set in kept) {
            return cache[set];
        }
        else {
            const temp = cb(set);
            kept[set] = temp;
            return true
        }
    }
    return inner;
}
module.exports = cacheFunction;